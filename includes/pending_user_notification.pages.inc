<?php

function pending_user_notifications_settings($form, &$form_state)
{
	$form['pun_show_empty_block'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Show block even when no users are pending'),
		'#default_value' => variable_get('pun_show_empty_block', 0),
	);

	return system_settings_form($form);
}
